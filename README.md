# README #

This is a proof of concept for using an ldap in a docker container as a SSO source for other containerised Django apps


### What is this repository for? ###

* Quick summary

This is a worked example combining the [Django_REST tutorial](https://www.django-rest-framework.org/tutorial/quickstart/) with JSON Web Tokens and access to users from a ldap
database. For simplicity of deploymemt the LDAP database itself is deployed as a Docker container using docker-compose.

Single sign on is demonstrated by the smple expedient of taking a copy of the tutorial tree and running the second instance at another port number.

The copy is needed so that both apps are using different sqlite databases so we can prove that single sign on is working via LDAP and not just that they are sharing a common user database.

Another useful feature of this demo is the generate_ldiff.pl program that populates the LDAP container with sample users. This could be adapted to take users and groups from some other
source as needed.

* Version

1 - March 2020

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

It is assumed you have the following aliases set for localhost in your /etc/hosts file. If not you will just have to ammend the code to refer to localhost
127.0.0.1	localhost ldap.aspiretechnology.com djangotest.aspiretechnology.com

An absolute minimum has been put in this repo.

** A docker.compose file that brings up the ldap container - be aware the ldap dissapears when you do docker-compose down, this is how we want it for testing but maybe not ideal in production!
** An ldap.env file that contains the fundamental setup for the OpenLdap server in the container.
** Empty ldap directory for loading data to ldap server.
** The generate_ldiff.pl script that populates the users in the ldap so we have users in different groups to test.
** requirements.txt that defines the Django software stack
** tutoral directory containing basic setup for the django app.

* Configuration

The python venv has not been included in this repo.

python3.8 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
cd tutorial
./manage.py migrate # this creates database
./manage.py createsuperuser --username admin --email admin@nowhere.com
# and give it password of your choice...
cd ..
cp -a tutorial tutorialclone # so we have two distinct databases

cd tutorial
./manage.py runserver_plus --cert-file cert.crt # this runs a server on port 8000

Now can sign in as admin with browser at https://djangotest.aspiretechnology.com:8000/admin

And on anther terminal
cd tutorialclone
./manage.py runserver_plus --cert-file cert.crt 0.0.0.0:9000

You can now use the same admin credentials to sign into https://djangotest.aspiretechnology.com:9000/admin

Note that only the admin iser is visible in each case.

Now we add the ldap:

docker-compose up -d
./generate_ldiff.pl # this adds the users - see script for details

# now we can test users of our choice
./withtok User44 changeme 9000
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   295  100   252  100    43   2964    505 --:--:-- --:--:-- --:--:--  3470
{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoic2xpZGluZyIsImV4cCI6MTU4MzM0MzQyMywianRpIjoiYzcyNTQ1M2YyMGNmNGExNjg0MmQwZWQ1NDQwZTZlMDUiLCJyZWZyZXNoX2V4cCI6MTU4MzQyOTgxMywidXNlcl9pZCI6Mn0.oa4L88qQmtzMY5LCOmH7scPflGINVRQAXnaZ8KFPpRE"} at ./withtok line 55.
using eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoic2xpZGluZyIsImV4cCI6MTU4MzM0MzQyMywianRpIjoiYzcyNTQ1M2YyMGNmNGExNjg0MmQwZWQ1NDQwZTZlMDUiLCJyZWZyZXNoX2V4cCI6MTU4MzQyOTgxMywidXNlcl9pZCI6Mn0.oa4L88qQmtzMY5LCOmH7scPflGINVRQAXnaZ8KFPpRE to 8000 at ./withtok line 17.
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   421  100   421    0     0   7517      0 --:--:-- --:--:-- --:--:--  7517
{
  'count' => 2,
  'next' => undef,
  'previous' => undef,
  'results' => [
                 {
                   'email' => 'User44@aspiretechnology.com',
                   'groups' => [
                                 'https://djangotest.aspiretechnology.com:8000/groups/1/',
                                 'https://djangotest.aspiretechnology.com:8000/groups/2/'
                               ],
                   'url' => 'https://djangotest.aspiretechnology.com:8000/users/2/',
                   'username' => 'user44'
                 },
                 {
                   'email' => 'admin@nowhere.com',
                   'groups' => [],
                   'url' => 'https://djangotest.aspiretechnology.com:8000/users/1/',
                   'username' => 'admin'
                 }
               ]
}
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    79  100    79    0     0   2393      0 --:--:-- --:--:-- --:--:--  2393
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    82  100    82    0     0   3037      0 --:--:-- --:--:-- --:--:--  3037
{
  'https://djangotest.aspiretechnology.com:8000/groups/1/' => {
                                                                'name' => 'Haiti',
                                                                'url' => 'https://djangotest.aspiretechnology.com:8000/groups/1/'
                                                              },
  'https://djangotest.aspiretechnology.com:8000/groups/2/' => {
                                                                'name' => 'Operator',
                                                                'url' => 'https://djangotest.aspiretechnology.com:8000/groups/2/'
                                                              }
}


What the withtok script does is signs in as the specified user and then fetches the data via the Django_REST API
This is the current users and groups this server knows about. The same information can be confirmed as the new users and any groups they are associated with now
also appear under the auth API.

The withtok script always goes to the port 8000 instance to get its token but the instance used to fetch data can be provided as the final parameter.
If we get a token from 8000 and use it on 9000 that proves SSO capability.

The one very minor issue is that each app must have existing local users corresponding to all LDAP users that want to use it. If this is not the case the Jason Web Token for that user is not
accepted. The way that this POC gets round this is by then getting a token from the second app.

An alternative would be some process that pre-populates all possible LDAP users into each app database. Note that Django no longer needs to keep passwords for these users. Setting a Django password
would turn a LDAP user into a local user for that application. Passwords used may start to diverge with confusion resulting!




Note requirements are not pinned to specific versions, this is for reasons of simplicity as this is just a POC, you may have to adjust this if there is a breaking change somewhere.

* Dependencies

see requirements.txt
* Database configuration

Sqlite databases insiade each application directory are used to store users. The point of this is that the apps do NOT share any database, just the LDAP.

* How to run tests
* Deployment instructions

Not applicable - this is just a POC designed so that the concepts should be easy to take and put into a real application.

### Contribution guidelines ###

* Writing tests

Ways to exercise the POC from other front ends welcome. Note only two URLs useable are /token to get the JWT and /users and /groups paths which are both driven by Django_REST routes.
The /admin url is the only part of the site useable from a browser. 
* Code review
* Other guidelines

If you have to pin to specific versions in requirements.txt please contribute that back. Initial release seems to work fine with all latest versions.

### Who do I talk to? ###

* martin.houston@aspiretechnology.com
