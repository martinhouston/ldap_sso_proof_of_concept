#!/usr/bin/perl

=head1 generate_ldiff

Generate basic set of ldiff data for OpenLDAP server in docker container and get it loaded.

=cut


my $container='aspire_openldap';
my $domain='dc=aspiretechnology,dc=com';
# some groups for users to belong to
my %role_groups = (
    Operator => {
	description=>'NOC operators'
    },
    Technician => {
	description=>'Technicians'
    },
    SoftwareDev => {
	description=>'Software Development'
    },
    Management => {
	description =>'Senior Management'
    }
    );

# And some locations so that in tool views can be limited to user's interests
my %location_groups = (
    Haiti => {
	description=>'Haiti market'
    },
    Jamaica => {
	description=>'Jamaica market'
    },
    Barbados => {
	description=>'Barbados market'
    },
    # and so on....

    # we could have a group to represent access all areas
    Everywhere => {
	description =>'All markets'
    }
);

# Give users ids starting at 1000
my $next_uid = 1000;  
# we organise this hash by cn of the user
my %users = (
    'Martin Houston' => {
			 uid => 'martinh',
			 uidNumber => $next_uid,
			 gidNumber => $next_uid,
			 homeDirectory => '/home/martinh',
			 givenName => 'Martin',
			 sn => 'Houston',
			 displayName => 'Martin Houston',
			 cn => 'Martin Houston',
			 mail => 'martin.houston@aspiretechnology.com',
			 telephoneNumber => '490855',
			 title => 'Linux consultant',
			 userPassword => slappasswd('changeme'),
			 role_groups => ['SoftwareDev'],
			 location_groups => ['Haiti','Jamaica'],
			 
			}
	    );
$next_uid++;
# make 50 test users, all Operator class
for my $usernum (1..50) {
  $users{"User$usernum"} = {
			    uid => "user$usernum",
			    uidNumber => $next_uid,
			    gidNumber => $next_uid,
			    homeDirectory => "/home/user$usernum",
			    givenName => 'Test',
			    sn => "User_$usernum",
			    displayName => "User $usernum",
			    cn => "User$usernum",
			    mail => "User$usernum\@aspiretechnology.com",
			    telephoneNumber => '123456',
			    title => "Dummy user $usernum",
			    userPassword => slappasswd('changeme'),
			    role_groups => ['Operator'],
			    # location at random
			    location_groups => [(keys %location_groups)[rand keys %location_groups]]
  };
  $next_uid++;
}

# Say first 10 users are management and can access all markets
for my $usernum(1..10) {
  $users{"User$usernum"}->{role_groups} =['Management'];
  $users{"User$usernum"}->{location_groups} = ['Everywhere'];

}

open(FD,"<ldap.env") or die "no ldap.env";

=pod 

This is what is in ldap.env (what the container uses to initialise)

LDAP_ORGANISATION=Aspire Technology Ltd.
LDAP_DOMAIN=aspiretechnology.com
LDAP_ADMIN_PASSWORD=password123xyz
LDAP_CONFIG_PASSWORD=password123xyz

LDAP_READONLY_USER=true
LDAP_READONLY_USER_USERNAME=ldap-ro
LDAP_READONLY_USER_PASSWORD=password123

=cut
  
my %ldap = ();
while(<FD>) {
    chomp;
    next if /^\s*#/; # ignore comments
    if(/=/) {
	my($k,$v) = split(/=/,$_,2);
	$ldap{$k} = $v;
    }
}

close FD;
open(FD,">ldap/00-startup.ldif") or die "ldap/00-startup.ldif creation not possible";

print FD qq{
dn: ou=users, $domain
ou: users
description: All people in $domain
objectclass: organizationalunit

dn: ou=groups, $domain
ou: groups
description: All groups in $domain
objectclass: organizationalunit

dn: ou=locationgroups, ou=groups, $domain
ou: locationgroups
description: Groups tieing a user to specific market access
objectclass: organizationalunit

dn: ou=rolegroups, ou=groups, $domain
ou: rolegroups
description: Groups tieing a user to job role
objectclass: organizationalunit


};

close FD;

open(FD,">ldap/01-groups.ldif") or die "ldap/01-groups.ldif creation not possible";

for my $g (sort keys %role_groups) {
  my $description = $groups{$g}->{description} || 'NO DESCRIPTION';
  print FD qq{
dn: cn=$g,ou=rolegroups,ou=groups,$domain
objectClass: groupOfUniqueNames
cn: $g
uniqueMember: cn=$g,ou=rolegroups,ou=groups,$domain
description: $description
};
  
}
for my $g (sort keys %location_groups) {
  my $description = $location_groups{$g}->{description} || 'NO DESCRIPTION';
  print FD qq{
dn: cn=$g,ou=locationgroups,ou=groups,$domain
objectClass: groupOfUniqueNames
cn: $g
uniqueMember: cn=$g,ou=locationgroups,ou=groups,$domain
description: $description
};

}

close FD;

# now the biggie the users

open(FD,">ldap/02-users.ldif") or die "ldap/02-users.ldif creation not possible";

for my $u (sort keys %users) {
  print FD qq{
dn: cn=$u,ou=users,$domain
objectClass: inetOrgPerson
objectClass: person
objectClass: organizationalPerson
objectClass: posixAccount
objectClass: top
};

  for my $k (sort keys %{$users{$u}}) {
    next if $k =~ /^(location|role)_groups/;
    print FD "$k: $users{$u}->{$k}\n";
  }
  # special handling needed for groups membership
  if(defined $users{$u}->{role_groups} && ref($users{$u}->{role_groups}) =~ /^ARRAY/) {
    for my $g (@{$users{$u}->{role_groups}}) {
      print FD qq{
dn: cn=$g,ou=rolegroups,ou=groups,$domain
changetype: modify
add: uniqueMember
uniquemember: cn=$u,ou=users,$domain
};
    }
  }
  if(defined $users{$u}->{location_groups} && ref($users{$u}->{location_groups}) =~ /^ARRAY/) {
    for my $g (@{$users{$u}->{location_groups}}) {
      print FD qq{
dn: cn=$g,ou=locationgroups,ou=groups,$domain
changetype: modify
add: uniqueMember
uniquemember: cn=$u,ou=users,$domain
};
    }
  }
}

close FD;

# Now we load the files

file_loader('00-startup.ldif');
file_loader('01-groups.ldif');
file_loader('02-users.ldif');

print "Search as user\n";
do_search('(objectclass=*)',"cn=Martin Houston,ou=users,$domain",'changeme');
print "Search as admin\n";
do_search();

for my $u (sort keys %users) {
  # password_changer($u);
}

sub file_loader {
  my $file = shift;
  my $passwd = $ldap{LDAP_ADMIN_PASSWORD};
  my $cmd = qq{docker exec $container ldapmodify -a -x -h localhost -p 389 -D "cn=admin,$domain" -f /data/ldif/$file -c -w $passwd};
  # run command returning the output
  print qx{$cmd};
}

# This will let a better password be chosen - but could just change
# it to a hash as output by slappasswd instead.
sub password_changer {
  my $user = shift;
  my $passwd = $ldap{LDAP_ADMIN_PASSWORD};
  my $cmd = qq{docker exec -it $container ldappasswd -x -h localhost -p 389 -D "cn=admin,$domain" -S "cn=$user,ou=users,$domain" -w $passwd};
  print $cmd,"\n";
}


sub do_search {
  my($criteria,$searchas,$passwd) = @_;
  my $criteria //= '(objectclass=*)';
  my $searchbase = $domain;
  if(defined $searchas) {
    $searchbase = $searchas; # non admin users can generally only see their own stuff
  }
  else {
    
    $searchas = "cn=admin,$domain";
  }
  $passwd //= $ldap{LDAP_ADMIN_PASSWORD}; # we know the admin password
  my $cmd = qq{docker exec $container ldapsearch -h localhost -p 389 -D "$searchas" -b "$searchbase" -s sub -w $passwd};
  print qx{$cmd},"\n";
}

# Use the slappasswd command in the container
sub slappasswd {
    my $userpasswd = shift;
    my $res = qx{docker exec $container slappasswd -h '{SSHA}' -s '$userpasswd'};
    chomp $res;
    return $res;
}
