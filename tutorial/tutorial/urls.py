"""tutorial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainSlidingView

# our app
from tutorial.quickstart import views

router = routers.DefaultRouter()
# r' means raw string - every character is literaly what you see - no backslash escapes.
# these are from the views imported from tutorial.quickstart
router.register(r'users', views.UserViewSet)
router.register(r'groups',views.GroupViewSet)


# wire up the API using automatic URL routing.
#Additionaly we include login URLs for the browseable API.

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/',include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    path('token',TokenObtainSlidingView.as_view()),
    
]
